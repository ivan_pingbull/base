  //sticky footer
let $ = require('jquery');

export default function() {
  let $body = $('body');
  let $window = $(window);
  let $globalWrapper = $('.global-wrapper');
  let footerSticky = function(){
    if($body.hasClass('footer-sticky')){
      $body.removeClass('footer-sticky');
    }
    let bodyHeight = $body.height();
    let globalWrapperHeight = $globalWrapper.addClass('check').height();
    if(globalWrapperHeight <= bodyHeight){
      $body.addClass('footer-sticky');
    }else{
      $body.removeClass('footer-sticky');
    }
    $globalWrapper.removeClass('check');
  }
  footerSticky();
  setTimeout(footerSticky, 3000);
  $window.on('resize', function() {
    footerSticky();
  });
};

