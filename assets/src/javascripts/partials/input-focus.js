jQuery(function($){

    /*WPADMINBAR*/
    var wpadminbar= $('#wpadminbar'),
        adminHeight = wpadminbar.outerHeight();

    // if(adminHeight === null){
        adminHeight=0;
    // }
    

     /*INPUT FOCUS*/

    function pseudoFixed(TrueOrFalse){
        if(TrueOrFalse==false){

            $body.removeClass('inputFocus');
            $header.css({
                "top": adminHeight + "px"
            });
            wpadminbar.css({
                "top": "0px"
            });
            $(window).on('scroll mousewheel', function(){
                $header.css({
                    "top": adminHeight + "px"
                });
                wpadminbar.css({
                    "top": "0px"
                });
            });
            $(window).unbind('touchmove');            
            

        } else{
            
            $body.addClass('inputFocus');
            $header.css({
                "top": $(window).scrollTop() + adminHeight + "px"
            });
            wpadminbar.css({
                "top": $(window).scrollTop() + "px"
            });
            $(window).on('scroll mousewheel', function(){
                $header.css({
                    "top": $(window).scrollTop() + adminHeight + "px"
                });
                wpadminbar.css({
                    "top": $(window).scrollTop() + "px"
                });
            });
            $(window).on('touchmove', function(event){
                event.preventDefault();
            });

        }
    }
    //opinion flexslider

    var regExp= new RegExp('break__' + '(\\d+)');
    var css_breakpoint_holder = $('#css-breakpoint-holder');
    if (css_breakpoint_holder.length == 0) $('<div id="css-breakpoint-holder"></div>').appendTo($body);
    css_breakpoint_holder = $('#css-breakpoint-holder');
    var breakpoint = $('#css-breakpoint-holder').css('font-family');
    if(parseInt(regExp.exec(breakpoint)[1]) == 1|| parseInt(regExp.exec(breakpoint)[1]) == 2){

        $('input, textarea').on('focus', function(){
            pseudoFixed();      
        });
        $('input, textarea').on('blur', function(){
            $body.removeClass('push-menu-open search-form-open');
            pseudoFixed(false);
            setTimeout(function(){
                $(window).scrollTop($(window).scrollTop()+1);
                $(window).scrollTop($(window).scrollTop()-1);
            }, 500)      
        });

        $(window).on('orientationchange', function(){
            $('input, textarea').blur();
            pseudoFixed(false);
        });

        $('input, textarea').on('click', function(event){
            event.stopPropagation();
        });

        $('input[type=submit]').on('focus', function(){
            pseudoFixed(false);
        });        
    }
  
});