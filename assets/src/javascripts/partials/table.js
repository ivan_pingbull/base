import $ from 'jquery';
export default function() {
    $('table').each(function () {
        var $table = $(this);

        $table.find('th').each(function (i) {
            var $th = $(this),
                label = $th.text();

            $table
                .find('tr').each(function () {
                    var $td = $(this).find('td').eq(i);

                    if (!!$.trim($td.text()).length) {
                        $td.attr('data-label', label);
                    } else {
                        $td.addClass('hidden');
                    }
                });
        });
    });
};
