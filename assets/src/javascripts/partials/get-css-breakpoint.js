import $ from 'jquery';
export let getCssBreakpoint = prefix => {
	let $body = $('body');
    let regex = new RegExp(prefix + '(\\d+)');
    let css_breakpoint;
    let css_breakpoint_holder = $('#css-breakpoint-holder');

    if(css_breakpoint_holder.length == 0) {$body.append('<div id="css-breakpoint-holder"></div>')}

    css_breakpoint_holder = $('#css-breakpoint-holder');
    css_breakpoint = css_breakpoint_holder.css('font-family');
    return regex.exec(css_breakpoint)[1];
};


