const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const debug = require('gulp-debug');
const path = require('../config.js');

module.exports = function() {
    return gulp.src(path.src.fonts, {since: gulp.lastRun('font')})
    	.pipe(plumber({
            errorHandler: notify.onError(err => ({
              title: 'fonts',
              message: err.message
            }))
          }))
        .pipe(debug({title: 'fonts:'}))
        .pipe(gulp.dest(path.built.fonts));
};