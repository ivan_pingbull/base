const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const debug = require('gulp-debug');
const imagemin = require('gulp-imagemin');
const path = require('../config.js');

module.exports = function() {
    return gulp.src(path.src.images, {since: gulp.lastRun('image')})
    	.pipe(plumber({
            errorHandler: notify.onError(err => ({
              title: 'images',
              message: err.message
            }))
          }))
        .pipe(debug({title: 'images:'}))
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeViewBox: false}]
        }))
        .pipe(gulp.dest(path.built.images));
};