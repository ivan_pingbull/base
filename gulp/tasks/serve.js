const browserSync = require('browser-sync').create();
const path = require('../config.js');

module.exports = function() {
	let serveFunction = () => {
		browserSync.init({
			server: {
			    baseDir: path.built.dir,
			    directory: true
			},
			tunnel: true,
			open: false
		});

		browserSync.watch(path.built.all).on('change', browserSync.reload);
	};
	return serveFunction();
};