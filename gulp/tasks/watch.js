const gulp = require('gulp');
const path = require('../config.js');

module.exports = function() {

    gulp.watch(path.watch.html, gulp.series('html'));

    gulp.watch(path.watch.images, gulp.series('image'));

    gulp.watch(path.watch.fonts, gulp.series('font'));

    gulp.watch(path.watch.sass, gulp.series('sass'));

};