const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const debug = require('gulp-debug');
const sass = require('gulp-sass');
const rucksack = require('gulp-rucksack');
const nano = require('gulp-cssnano');
const path = require('../config.js');

module.exports = function() {
    return gulp.src(path.src.sass)
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
              title: 'sass',
              message: err.message
            }))
          }))
        .pipe(debug({title: 'sass:'}))
        .pipe(sass().on('error', sass.logError))
        .pipe(rucksack({
            autoprefixer: true
        }))
        // .pipe(nano({zindex: false}))
        .pipe(gulp.dest(path.built.css));
};