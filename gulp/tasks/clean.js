const del = require('del');
const path = require('../config.js');

module.exports = function() {
    return del(path.built.dir);
};