const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const debug = require('gulp-debug');
const fileinclude = require('gulp-file-include');
const path = require('../config.js');

module.exports = function() {
    return gulp.src(path.src.html)
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
              title: 'html',
              message: err.message
            }))
          }))
        .pipe(debug({title: 'html:'}))
        .pipe(fileinclude({
          prefix: '@@',
          basepath: '@file'
        }))
        .pipe(gulp.dest(path.built.html));
};