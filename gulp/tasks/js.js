const gulp = require('gulp');
const browserify = require('browserify');
const babelify = require('babelify');
const watchify = require('watchify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const path = require('../config.js');

module.exports = function(callback) {
    let bundle = () => {
        let bundler = browserify({
            entries: [path.src.js],
            cache: {},
            packageCache: {},
            plugin: callback ? [watchify]: ''
        }).transform(babelify, {
            presets: ['es2015'],
            plugins: [
               ['transform-es2015-classes', {
                   loose: true 
               }]
           ]
        });
        let rebundle = () => {
            let result = bundler
                .bundle()
                .on('error', console.log.bind(console, 'Browserify Error'))
                .pipe(source('all.js'))
                .pipe(buffer());

            return result
                .pipe(gulp.dest(path.built.js))
                .pipe(uglify())
                .pipe(rename({ suffix: '.min' }))
                .pipe(gulp.dest(path.built.js));
        };

        bundler
            .on('update', () => {
                console.time('Rebundle');
                rebundle()
                    .on('end', () => console.timeEnd('Rebundle'));
            })
            .on('log', console.log);

        return rebundle();
    };

    return bundle();
};

