module.exports = {
    src: {
        dir:                    'assets/src/',
        fonts:                  'assets/src/fonts/**/*.{eot,ttf,woff,woff2,svg}',
        js:                     'assets/src/javascripts/all.js',
        sass:                   'assets/src/sass/*.sass',
        images:                 'assets/src/images/**/*.{jpg,png,gif,svg}',
        html:                   'assets/src/html/*.html'
    },

    watch: {
        fonts:                  'assets/src/fonts/**/*.{eot,ttf,woff,woff2,svg}',
        js:                     'assets/src/javascripts/**/*.js',
        sass:                   'assets/src/sass/**/*.sass',
        html:                   'assets/src/html/**/*.html',
        images:                 'assets/src/images/**/*.{jpg,png,gif,svg}'
    },

    built: {
        dir:                    'assets/built/',
        fonts:                  'assets/built/fonts/',
        js:                     'assets/built/javascripts/',
        css:                    'assets/built/stylesheets/',
        html:                   'assets/built/html',
        images:                 'assets/built/images/',
        all:                    'assets/built/**/*.*'
    }
};