'use strict';

const gulp = require('gulp');


function lazyRequireTask(taskName, path, callback) {
  gulp.task(taskName, function() {
  	let task = require(path);
    return task(callback);
  });
}

lazyRequireTask('clean', './gulp/tasks/clean');

lazyRequireTask('font', './gulp/tasks/font');

lazyRequireTask('image', './gulp/tasks/image');

lazyRequireTask('html', './gulp/tasks/html');

lazyRequireTask('sass', './gulp/tasks/sass');

lazyRequireTask('watch', './gulp/tasks/watch');

lazyRequireTask('serve', './gulp/tasks/serve');

lazyRequireTask('js', './gulp/tasks/js', false);

lazyRequireTask('js:watch', './gulp/tasks/js', true);

gulp.task('build', gulp.parallel('font', 'html', 'image', 'sass', 'js'));

gulp.task('rebuild', gulp.series('clean', 'build'));

gulp.task('default', gulp.series('rebuild', 'js:watch', gulp.parallel('watch', 'serve')));

